# EASY

# Define a method that, given a sentence, returns a hash of each of the words as
# keys with their lengths as values. Assume the argument lacks punctuation.
def word_lengths(str)
  words = str.split
  counter = Hash.new(0)

  words.each do |word|
    counter[word] = word.length
  end
  counter
end

# Define a method that, given a hash with integers as values, returns the key
# with the largest value.
def greatest_key_by_val(hash)
  hash.sort_by{|k, v| v}[-1][0]
end

# Define a method that accepts two hashes as arguments: an older inventory and a
# newer one. The method should update keys in the older inventory with values
# from the newer one as well as add new key-value pairs to the older inventory.
# The method should return the older inventory as a result. march = {rubies: 10,
# emeralds: 14, diamonds: 2} april = {emeralds: 27, moonstones: 5}
# update_inventory(march, april) => {rubies: 10, emeralds: 27, diamonds: 2,
# moonstones: 5}
def update_inventory(older, newer)
  newer.each do |k, v|
    older[k] = v
  end
  older
end

# Define a method that, given a word, returns a hash with the letters in the
# word as keys and the frequencies of the letters as values.
def letter_counts(word)
  count = Hash.new(0)

  word.each_char do |ch|
    count[ch] += 1
  end
  count
end

# MEDIUM

# Define a method that, given an array, returns that array without duplicates.
# Use a hash! Don't use the uniq method.
def my_uniq(arr)
  count = Hash.new(0)
  arr.each do |el|
    count[el] += 1
  end
  count.keys
end

# Define a method that, given an array of numbers, returns a hash with "even"
# and "odd" as keys and the frequency of each parity as values.
def evens_and_odds(numbers)
  parity = Hash.new(0)

  numbers.each do |num|
    if num.even?
      parity[:even] += 1
    else
      parity[:odd] += 1
    end

  end
  parity
end

# Define a method that, given a string, returns the most common vowel. If
# there's a tie, return the vowel that occurs earlier in the alphabet. Assume
# all letters are lower case.
def most_common_vowel(string)
  vowels = "aeiou"
  just_vowels = string.chars.select{|ch| vowels.include?(ch)}
  vowel_frequency = Hash.new(0)

  just_vowels.each do |ch|
      vowel_frequency[ch] += 1
  end
  vowel_frequency.sort_by{|k, v| v}[-1][0]
end

# HARD

# Define a method that, given a hash with keys as student names and values as
# their birthday months (numerically, e.g., 1 corresponds to January), returns
# every combination of students whose birthdays fall in the second half of the
# year (months 7-12). students_with_birthdays = { "Asher" => 6, "Bertie" => 11,
# "Dottie" => 8, "Warren" => 9 }
# fall_and_winter_birthdays(students_with_birthdays) => [ ["Bertie", "Dottie"],
# ["Bertie", "Warren"], ["Dottie", "Warren"] ]
def fall_and_winter_birthdays(students)
  fall_and_winter = students.select{|k, v| v >= 7 }.keys
  combinations = []
  #fall_and_winter_students.each_with_index do |name, idx|
  fall_and_winter.each_index do |idx|
    ((idx +1)...fall_and_winter.length).each do |idx2|
     combinations << [fall_and_winter[idx], fall_and_winter[idx2]]
    end
  end
  combinations
end

# Define a method that, given an array of specimens, returns the biodiversity
# index as defined by the following formula: number_of_species**2 *
# smallest_population_size / largest_population_size biodiversity_index(["cat",
# "cat", "cat"]) => 1 biodiversity_index(["cat", "leopard-spotted ferret",
# "dog"]) => 9
def biodiversity_index(specimens)
  species = Hash.new(0)

  specimens.each do |animal|
    species[animal] += 1
  end

  ((species.length**2) * (species.values.min)) / species.values.max

end

# Define a method that, given the string of a respectable business sign, returns
# a boolean indicating whether pranksters can make a given vandalized string
# using the available letters. Ignore capitalization and punctuation.
# can_tweak_sign("We're having a yellow ferret sale for a good cause over at the
# pet shop!", "Leopard ferrets forever yo") => true
def can_tweak_sign?(normal_sign, vandalized_sign)
  normal_hash = character_count(normal_sign)
  vandalized_hash = character_count(vandalized_sign)

  vandalized_hash.all? do |char, count|
    normal_hash[char.downcase] >= count
  end
end

def character_count(str)
  count = Hash.new(0)
  str.each_char do |ch|
    next if ch == " "
    count[ch.downcase] += 1
  end
  count
end
